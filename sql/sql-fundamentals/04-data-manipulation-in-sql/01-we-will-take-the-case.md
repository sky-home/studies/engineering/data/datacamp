# We Will Take the CASE

## Prerequisites

You must know:
- `SELECT` statement
- `GROUP BY` statement
- `WHERE` clause
- `LEFT JOIN`
- `RIGHT JOIN`
- `INNER JOIN`

## We Will Take the CASE

`CASE` statement contains a `WHEN`, `THEN` and `ELSE` statement, finished with `END`.

```sql
CASE 
  WHEN x = 1 THEN 'a'
  WHEN x = 2 THEN 'b'
  ELSE 'c'
END AS new_column
```

> Do not forget the `END` term and add alias after it.

The completed `CASE` statement will evaluate to one column in your SQL query.

```sql
SELECT
  date,
  home_goal,
  away_goal,
  CASE 
    WHEN home_goal > away_goal THEN 'Home Team Win'
    WHEN home_goal < away_goal THEN 'Away Team Win'
    ELSE 'Tie'
  END AS outcome
FROM match
WHERE season = '2013/2014';
```

## In CASE Things Get More Complex

We can use multiple logical conditions on `WHEN`, using `AND`.

```sql
SELECT
  date,
  home_goal,
  away_goal,
  CASE 
    WHEN hometeam_id = 8455 AND home_goal > away_goal 
      THEN 'Chelsea Home Win'
    WHEN awayteam_id = 8455 AND home_goal < away_goal 
      THEN 'Chelsea Away Win'
  END AS outcome
FROM match
WHERE hometeam_id = 8455 OR awayteam_id = 8455;
```

SQL code above can return NULL. To avoid that, we can filter the `CASE` statement in `WHERE` clause.

```sql
SELECT
  date,
  season,
  CASE 
    WHEN hometeam_id = 8455 AND home_goal > away_goal 
      THEN 'Chelsea Home Win'
    WHEN awayteam_id = 8455 AND home_goal < away_goal 
      THEN 'Chelsea Away Win'
  END AS outcome
FROM match
WHERE 
  CASE 
    WHEN hometeam_id = 8455 AND home_goal > away_goal 
      THEN 'Chelsea Home Win'
    WHEN awayteam_id = 8455 AND home_goal < away_goal 
      THEN 'Chelsea Away Win'
  END IS NOT NULL;
```

## CASE WHEN with Aggregate Functions

Beside of categorizing and filtering data, `CASE` also can be use for aggregating data.

### With COUNT

```sql
SELECT
  season,
  COUNT(CASE
          WHEN hometeam_id = 8650 
            AND home_goal > away_goal 
              THEN id
        END) AS home_wins,
  COUNT(CASE
          WHEN awayteam_id = 8650 
            AND away_goal > home_goal 
              THEN id
        END) AS away_wins
FROM match 
GROUP BY season;
```

> You can return anything from `CASE` statement inside `COUNT()`.

### With SUM

```sql
SELECT
  season,
  SUM(CASE
        WHEN hometeam_id = 8650
          THEN home_goal
      END) AS home_goals,
  SUM(CASE
        WHEN awayteam_id = 8650
          THEN away_goal
      END) AS away_goals
FROM match
GROUP BY season;
```

### With AVG

```sql
SELECT
  season,
  AVG(CASE
        WHEN hometeam_id = 8650
          THEN home_goal
      END) AS home_goals,
  AVG(CASE
        WHEN awayteam_id = 8650
          THEN away_goal
      END) AS away_goals
FROM match
GROUP BY season;
```

> You also can use `ROUND()` to make it easy to read.

You also can count percentages with `AVG()` and `CASE`.

```sql
SELECT 
  season,
  AVG(CASE
        WHEN hometeam_id = 8455
          AND home_goal > away_goal 
        THEN 1 
        WHEN hometeam_id = 8455
          AND home_goal < away_goal 
        THEN 0
      END) AS pct_homewins,
  AVG(CASE
        WHEN awayteam_id = 8455
          AND away_goal > home_goal 
        THEN 1 
        WHEN awayteam_id = 8455
          AND away_goal < home_goal 
        THEN 0
      END) AS pct_awaywins
FROM match
GROUP BY season;
```
