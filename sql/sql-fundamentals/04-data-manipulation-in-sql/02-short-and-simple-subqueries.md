# Short and Simple Subqueries

## WHERE Are the Subqueries?

Subquery is query _nested_ inside another query. Can be placed in `SELECT`, `FROM`, `WHERE`, and `GROUP BY`. Subquery can return scalar quantities, a list, or a table. 

Subquery can be used to:
- Comparing group to sumarizing values
- Make a better structure or reshape your data
- Combining data that cannot be joined

### Simple Subquery

- Can be evaluated independently from the outer query
- Is only processed once in the entire statement

```sql
SELECT home_goal
FROM match
WHERE home_goal >
  (SELECT AVG(home_goal)
  FROM match);
```

> Subqueries are executed first.

#### Subqueries in WHERE

```sql
SELECT
  team_long_name,
  team_short_name AS abbr
FROM team
WHERE  team_api_id IN  
  (SELECT hometeam_id
  FROM match
  WHERE country_id = 15722);
```

## Subqueries in FROM

Subqueries in `FROM` can be used for:
- Restructure and transform your data
  - Transforming data from long to wide before selecting
  - Prefiltering data
- Calculating aggregates of aggregates

```sql
SELECT
  team,
  home_avg
FROM (SELECT
        t.team_long_name AS team,
        AVG(m.home_goal) AS home_avg
      FROM match AS m
      LEFT JOIN team AS t
        ON m.hometeam_id = t.team_api_id
      WHERE season = '2011/2012'
      GROUP BY team) AS subquery
ORDER BY home_avg DESC
LIMIT 3;
```

> Remember to make an alias for each subquery inside `FROM`.

> You also able to join table inside `FROM` with table from subquery.

## Subqueries in SELECT

Subquery in `SELECT` is used to return a single aggregate value.

```sql
SELECT
  date,
  (home_goal + away_goal) AS goals,
  (home_goal + away_goal) -
    (SELECT AVG(home_goal + away_goal)
    FROM match
    WHERE season = '2011/2012') AS diff
FROM match
WHERE season = '2011/2012';
```

Note that in SQL code above, the subquery also have its own `WHERE` clause, that because the subquery is being executed first.

> Subquery in `SELECT` must return one record.

## Best Practice

- Line up your code
- Annotate your queries using comments
  ```sql
  -- in line comment
  /*
  multiline comment
  */
  ```
- Indent your queries
