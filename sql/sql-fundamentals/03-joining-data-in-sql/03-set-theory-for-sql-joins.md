# Set Theory for SQL Joins

## Set Theory for SQL Joins

SQL have 3 set operations `UNION`, `INTERSECT`, `EXCEPT`.

![set operations](./assets/set-operations.png)

> This set operations is very useful when we have 2 tables with exact same data types.

## UNION and UNION ALL

```sql
SELECT *
FROM table1
UNION
SELECT *
FROM table2;
```

`UNION` takes two tables as input and returns all records from both tables.

> If there are 2 records identical, `UNION` will only return one.

```sql
SELECT *
FROM table1
UNION ALL
SELECT *
FROM table2;
```

`UNION ALL` include duplicate records.

> For all set operations, the number of selected fields and their data types must be identical.

> The result field name will be from the first `SELECT` statement.

## INTERSECT

`INTERSECT` takes 2 tables as input, and returns only the records that exist in both tables.

```sql
SELECT id, name
FROM table1
INTERSECT
SELECT id, name
FROM table2;
```

> The difference between `INTERSECT` and `INNER JOIN` is that `INNER JOIN` also returns duplicate records, while `INTERSECT` not.

## EXCEPT

`EXCEPT` will returns all records on the left table that not matches to records on the right table.

```sql
SELECT id, name
FROM table1
EXCEPT
SELECT id, name
FROM table2;
```
