# Outer Joins, Cross Joins and Self Joins

## LEFT and RIGHT JOINs

### LEFT JOIN

![left join](./assets/left-join.png)

`LEFT JOIN` will return all records from the left table and all the matching record in the right table.

```sql
SELECT t1.name, t2.address
FROM table1 AS t1
LEFT JOIN table2 AS t2
USING(id);
```

> `LEFT JOIN` also can be written `LEFT OUTER JOIN`.

### RIGHT JOIN

![right join](./assets/right-join.png)

`RIGHT JOIN` is the reversed of `LEFT JOIN`.

```sql
SELECT t1.name, t2.address
FROM table1 AS t1
RIGHT JOIN table2 AS t2
USING(id);
```

> `RIGHT JOIN` also can be written `RIGHT OUTER JOIN`.

> `RIGHT JOIN` is also able to be rewritten as `LEFT JOIN`.

## FULL JOINs

![full join](./assets/full-join.png)

A `FULL JOIN` combines a `LEFT JOIN` and `RIGHT JOIN`.

```sql
SELECT t1.name, t2.address
FROM table1 AS t1
FULL JOIN table2 AS t2
USING(id);
```

> `FULL JOIN` also can be written `FULL OUTER JOIN`.

## Crossing Into CROSS JOIN

![cross join](./assets/cross-join.png)

`CROSS JOIN` return all possible combination of each table.

```sql
SELECT t1.id, t2.address
FROM table1 AS t1
CROSS JOIN table2 AS t2
```

> `CROSS JOIN` does not need `ON` clause.

## Self Joins

```sql
SELECT 
  t1.name AS name1, 
  t2.name AS name2,
  t1.class
FROM table1 AS t1
INNER JOIN table1 AS t2
USING(id);
```

> We need to use aliases in `SELF JOIN`.

We can remove the same name pair with `<>`.

```sql
SELECT 
  t1.name AS name1, 
  t2.name AS name2,
  t1.class
FROM table1 AS t1
INNER JOIN table1 AS t2
ON t1.id = t2.id
  AND t1.name <> t2.name;
```
