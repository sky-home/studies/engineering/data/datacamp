# Introducing Inner Joins

## The Ins and Outs of INNER JOIN

We can see all the matching `key` in 2 or more table using `INNER JOIN`.

![inner join](./assets/inner-join.png)

```sql
SELECT table1.name, table2.address
FROM table1
INNER JOIN table2
ON table1.id = table2.id;
```

> Write the `JOIN` clause first then `SELECT` clause as best practice.

We can simplify the SQL code above using `USING`.

```sql
SELECT table1.name, table2.address
FROM table1
INNER JOIN table2
USING(id);
```

> `USING` only works when the field name of each table is the same.

We also able to make an alias to each table.

```sql
SELECT t1.name, t2.address
FROM table1 AS t1
INNER JOIN table2 AS t2
ON t1.id = t2.id;
```

## Defining Relationships

### One-to-many Relationships

![one to many relationship](./assets/one-to-many-relationship.png)

One to many relationship is the most common type or relationship, where a single entity can be associated with several entity. As example, in a library on author is able to make one or more books.

### One-to-one Relationships

![one to one relationship](./assets/one-to-one-relationship.png)

One student can only have one student id.

### Many-to-many Relationships

![many to many relationship](./assets/many-to-many-relationship.png)

A country can have multiple languages.

## Multiple Joins

We also able to make a multiple joins.

```sql
SELECT t1.name, t2.address
FROM table1 AS t1
INNER JOIN table2 AS t2
ON t1.id = t2.id
INNER JOIN table3 AS t3
ON t1.id = t3.id;
```

> We can use table1 or table2 in the second `JOIN` clause.

We also able to use multiple fields in one `JOIN` clause.

```sql
SELECT t1.name, t2.address
FROM table1 AS t1
INNER JOIN table2 AS t2
ON t1.id = t2.id
  AND t1.code = t2.code;
```
