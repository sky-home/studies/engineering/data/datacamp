# Subqueries

## Subquerying with semi joins and anti joins

### Semi Joins

A semi join chooses records in the first table where a condition is met in the second table.

```sql
SELECT name, address
FROM table1
WHERE name IN
  (SELECT name
  FROM table2
  WHERE year_in > 2010);
```

> SQL code inside the parenthesis is called subquery.

### Anti Joins

The anti join chooses records in the first table where field1 does NOT find a match in field2.

```sql
SELECT name, address
FROM table1
WHERE name NOT IN
  (SELECT name
  FROM table2
  WHERE year_in > 2010);
```

> There are no new fields added in semi join or anti join.

## Subqueries inside WHERE and SELECT

### In WHERE

`WHERE` clause in the most common place for subqueries because of filtering data.

```sql
SELECT name, address
FROM table1
WHERE name IN
  (SELECT full_name
  FROM table2
  WHERE year_in > 2010);
```

> The type of field name in `WHERE` clause must the same as the type of full_name field.

> The subquery can be from the same table or another table.

### In SELECT

```sql
SELECT 
  DISTINCT class,
  (SELECT COUNT(*)
  FROM table2
  WHERE table1.id = table2.class_id) AS student_count
FROM table1;
```

> Subqueries inside `SELECT` statement requires an alias.

## Subqueries inside FROM

We can use more than 1 table inside `FROM` clause.

```sql
SELECT t1.id, t2.name
FROM table1 AS t1, table2 AS t2
WHERE t1.id = t2.id;
```

SQL code above also returns duplicate records. We can omit that with `DISTINCT`.

```sql
SELECT DISTINCT t1.id, t2.name
FROM table1 AS t1, table2 AS t2
WHERE t1.id = t2.id;
```

```sql
SELECT DISTINCT t1.class, t2.most_recent
FROM table1 AS t1,
  (SELECT 
    class,
    MAX(year_in) AS most_recent
  FROM table2
  GROUP BY class) AS sub
WHERE t1.class = sub.class
ORDER BY class;
```
