# Sorting and Grouping

## Sorting Results

We can use `ORDER BY` to sort results.

```sql
SELECT title, budget
FROM films
ORDER BY budget;
```

> By default ORDER BY will order the result as ascending.

We can use `ASC` or `DESC` to clarify the sorting order.

```sql
SELECT title, budget
FROM films
ORDER BY budget DESC;
```

We also can sort by multiple fields. The sorting order will be from the left first. We are also able to use a different ordering oeder.

```sql
SELECT title, budget
FROM films
ORDER BY title, budget DESC;
```

### Order of Execution

If we have a sql code like this.

```sql
SELECT title
FROM films
WHERE country = 'Japan'
ORDER BY release_year
LIMIT 5;
```

So, the order of execution will be FROM, WHERE, SELECT, ORDER BY, and LIMIT.

## Grouping Data

We are able to group result with `GROUP BY` clause.

```sql
SELECT certification, COUNT(title) AS title_count
FROM films
GROUP BY certification;
```

> `ORDER BY` usually used with aggregate functions.

> Will be an error if we try to `SELECT` fields that is not in our `GROUP BY` clause.

We are also able to use `ORDER BY` clasue together with `ORDER BY`.

```sql
SELECT certification, COUNT(title) AS title_count
FROM films
GROUP BY certification
ORDER BY title_count DESC;

```

### Order of Execution

If we have a sql code like this.

```sql
SELECT certification, COUNT(title) AS title_count
FROM films
WHERE country = 'Japan'
GROUP BY certification
ORDER BY release_year
LIMIT 5;
```

So, the order of execution will be FROM, WHERE, GROUP BY, SELECT, ORDER BY, and LIMIT.

## Filtering Grouped Data

We can filter the grouped result in `HAVING` clause.

```sql
SELECT
  release_year,
  COUNT(title) AS title_count
FROM films
GROUP BY release_year
HAVING COUNT(title) > 10;
```

### Order of Execution

If we have a sql code like this.

```sql
SELECT certification, COUNT(title) AS title_count
FROM films
WHERE country = 'Japan'
GROUP BY certification
HAVING COUNT(title) > 100
ORDER BY release_year
LIMIT 3;
```

So, the order of execution will be FROM, WHERE, GROUP BY, HAVING, SELECT, ORDER BY, and LIMIT.

> In PostgreSQL, you can use aliases from `SELECT` statement in `GROUP BY` clause.
