# Aggregate Function

## Summarizing Data

Aggregate function return a single value. We already knew 1 aggregate function (`COUNT`) and there are 4 additional aggregate function.

- AVG()

```sql
SELECT AVG(budget)
FROM films;
```

- SUM()

```sql
SELECT SUM(budget)
FROM films;
```

- MIN()

```sql
SELECT MIN(budget)
FROM films;
```

- MAX()

```sql
SELECT MAX(budget)
FROM films;
```

> AVG() and SUM() is only for numerical values.  
> COUNT(), MIN(), MAX() is able to be used in non-numerical values.


> Do not forget to use aliases when using aggregate function.

## Summarizing Subsets

We can use `WHERE` with aggregate function, because `WHERE` is executed before `SELECT`.

```sql
SELECT AVG(budget) AS avg_budget
FROM films
WHERE release_year >= 2010;
```

We can round a number using `ROUND()` function.

> ROUND() takes 2 arguments. ROUND(number_to_round, decimal_places).  
> The second argument is optional.

> Only can be used on numerical values.

```sql
SELECT ROUND(AVG(budget), 2) AS avg_budget
FROM films
WHERE release_year >= 2010;
```

## Aliasing and Arithmetic

We can perform basic arithmetic with symbols, like `+`, `-`, `*`, and `/`.

> Use parentheses when performing arithmetic.  
> Also use decimal while performing division if you want the exact result.

```sql
SELECT (4 + 5);
```

> Aggregate function perform operations vertically(field/column).  
> While arithmetic horizontally(record/row).

Also do not forget to use Aliases when performing arithmetic.

```sql
SELECT (gross - budget) AS profit
FROM films;
```
