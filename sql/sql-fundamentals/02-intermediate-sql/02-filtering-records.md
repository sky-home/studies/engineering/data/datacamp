# Filtering Records

## Filtering Numbers

To filter the records, we can use `WHERE`.

```sql
SELECT title
FROM films
WHERE release_year = 1960;
```

We can use comparison operators to filter numbers, such as :

| Operators | Desc |
| :-------- | :--- |
| =         | Equal to |
| <         | Less than |
| >         | Greater than |
| <=        | Equal to or less than |
| >=        | Equal to or greater than |
| <>        | Not equal to |

We can also filter string using `equal to` or `=` sign.

```sql
SELECT title
FROM films
WHERE country = 'Japan';
```

> To filter a string, we must use a single quotation mark `''`

### Order of Execution

If we have a sql code like this.

```sql
SELECT title
FROM films
WHERE country = 'Japan'
LIMIT 5;
```

So, the order of execution will be FROM, WHERE, SELECT, and LIMIT.

## Multiple Criteria

To filter with multiple criteria, we have 3 additional keywords.

- OR

```sql
SELECT *
FROM coats
WHERE color = 'yellow' OR length = 'short';
```

- AND

```sql
SELECT *
FROM coats
WHERE color = 'yellow' AND length = 'short';
```

- BETWEEN

```sql
SELECT *
FROM coats
WHERE buttons BETWEEN 1 AND 5;
```

We can use multiple keywords at once. as example:

```sql
SELECT title
FROM films
WHERE (release_year = 1994 OR release_year = 2000)
  AND (certification = 'PG' OR certification = 'R');
```

> We might use parentheses to make a correct execution order

We also can use `BETWEEN` to shorten an `AND` expression.

```sql
SELECT title
FROM films
WHERE release_year >= 1994 
  AND release_year <= 2000);
```

to

```sql
SELECT title
FROM films
WHERE release_year 
  BETWEEN 1994 AND 2000;
```

## Filtering Text

To filter a pattern of text we need this 3 additional keywords.

- LIKE

> Use wildcard as placeholder

There are 2 wildcards, % (percent) and _ (underscore).

% used to match many characters.

```sql
SELECT name
FROM people
WHERE name LIKE '%ye%';
```

_ used to match a single character.

```sql
SELECT name
FROM people
WHERE name LIKE 'Sk_';
```

- NOT LIKE

```sql
SELECT name
FROM people
WHERE name NOT LIKE 'Sk%';
```

- IN

```sql
SELECT title
FROM film
WHERE release_year IN (1920, 1930, 1940);
```

```sql
SELECT title
FROM films
WHERE country IN ('Germany', 'France');
```

## NULL Values

NULL represents a missing or unknown values.

To see a record that is missing, we can use `IS NULL` keyword.

```sql
SELECT name
FROM people
WHERE birthday IS NULL;
```

We also can use `IS NOT NULL` keyword to filter records that is not having missing values.

```sql
SELECT COUNT(name) AS count_birthdates
FROM people
WHERE birthday IS NOT NULL;
```

> COUNT() is used to count NOT NULL values.
